/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2011 SMUNIX
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Providence M. Salumu <providence.salumu@smunix.com>
 */

#include "test.hpp"
#include <slib/singleton.hpp>

struct DataUniq : public smunix::Singleton<DataUniq>
{  
  int mValue;
  int Value() const { return mValue; }
  void Value(int val) { mValue = val; }
};

struct PointerUniq : public smunix::Singleton<PointerUniq, std::shared_ptr<PointerUniq> >
{
  int mValue;
  int Value() const { return mValue; }
  void Value(int val) { mValue = val; }
};

struct SingletonFixture 
{
  SingletonFixture (void)
  {
    DataUniq::Instance ().Value (0);
    PointerUniq::Instance ()->Value (0);
  }
  ~SingletonFixture()
  {
    DataUniq::Instance ().Value (-1);
    PointerUniq::Instance ()->Value (-1);
  }

};

BOOST_FIXTURE_TEST_SUITE (, SingletonFixture)

BOOST_AUTO_TEST_CASE (IsUnique1)
{
  BOOST_CHECK_EQUAL (DataUniq::Instance ().Value (), 0);
  DataUniq::Instance().Value(1);
  BOOST_CHECK_EQUAL (DataUniq::Instance ().Value (), 1);

  BOOST_CHECK_EQUAL (PointerUniq::Instance ()->Value (), 0);
  PointerUniq::Instance()->Value(1);
  BOOST_CHECK_EQUAL (PointerUniq::Instance ()->Value (), 1);
}

BOOST_AUTO_TEST_CASE (IsUnique2)
{
  DataUniq::Instance().Value(3);
  BOOST_CHECK_EQUAL (DataUniq::Instance ().Value (), 3);

  PointerUniq::Instance()->Value(3);
  BOOST_CHECK_EQUAL (PointerUniq::Instance ()->Value (), 3);
}
BOOST_AUTO_TEST_SUITE_END ()

