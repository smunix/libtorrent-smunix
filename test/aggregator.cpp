/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2011 SMUNIX
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Providence M. Salumu <providence.salumu@smunix.com>
 */


#include "test.hpp"
#include <slib/aggregator.hpp>
#include <slib/singleton.hpp>
#include <memory>

struct IOne
{
  virtual ~IOne (void) {}
  virtual void One (void) const = 0;
};

struct ITwo
{
  virtual ~ITwo (void) {}
  virtual void Two (void) const = 0;
};

template<class I>
void ConstRefCall (I const &i)
{
  (void)i;
  BOOST_TEST_MESSAGE (BOOST_CURRENT_FUNCTION);
}

template<class I>
void RefCall (I &i)
{
  (void)i;
  BOOST_TEST_MESSAGE (BOOST_CURRENT_FUNCTION);
}

struct Callback : public smunix::Singleton<Callback>
{
  Callback () : mValue (0) {}
  int mValue;
  int Value() const { return mValue; }
  void Value(int val) { mValue = val; }
};

struct OneType : public IOne, public smunix::Aggregated<ITwo>, public smunix::AggregatedPeeker<OneType>
{
  typedef std::shared_ptr<OneType> SP;
  virtual ~OneType (void) {}
  virtual void One (void) const
  {
    Callback::Instance ().Value (1 + Callback::Instance ().Value ());
  }
  void Invoke (void) const
  {
    Peek<ITwo> ()->Two ();
  }
};

struct TwoType : public ITwo, public smunix::Aggregated<IOne>, public smunix::AggregatedPeeker<TwoType>
{
  typedef std::shared_ptr<TwoType> SP;
  virtual ~TwoType (void) {}
  virtual void Two (void) const
  {
    Callback::Instance ().Value (1 + Callback::Instance ().Value ());
  }
  void Invoke (void) const
  {
    Peek<IOne> ()->One ();
  }
};

#define  KLAZZ(x, b) \
  struct x : public BOOST_PP_CAT(I, b)\
  { \
    int _; int &_r; x () : _ (0), _r (_)  {} x(int &p) : _(0), _r (p) {} virtual ~x (void) {} \
    virtual void b (void) const { const_cast<int &>(_r)++; } \
    int operator() (void) const {return _r;} \
  };

KLAZZ(My1, One)
KLAZZ(My2, Two)

struct AllInOne :
  public smunix::AggregatedPeeker<AllInOne>,
  public smunix::Aggregated<IOne>,
  public smunix::Aggregated<ITwo>
{
    virtual ~AllInOne (void) {}
    void Do () const
    {
      Peek<IOne> ()->One ();
      Peek<ITwo> ()->Two ();
    }
};

struct AllInOneWithAggregator : public smunix::Aggregator<AllInOneWithAggregator, IOne, ITwo>
{
  virtual ~AllInOneWithAggregator (void) {}
  void Do () const
  {
    Peek<IOne> ()->One ();
    Peek<ITwo> ()->Two ();
  }
};

struct Fixture
{
  Fixture (void) : value (0)
  {
    one.reset (new OneType);
    two.reset (new TwoType);

    one->Pack<ITwo> (two);
    two->Pack<IOne> (one);

    my1 = new My1;
    my2 = new My2;

    allInOne.Pack<IOne> (my1);
    allInOne.Pack<ITwo> (my2);

    allInOneAggregator.Pack<IOne> (new My1 (value));
    allInOneAggregator.Pack<ITwo> (new My2 (value));
  }
  virtual ~Fixture (void)
  {
    one.reset ();
    two.reset ();

    /**
     * Look out! Don't call delete on my1 nor my2 as their ownership
     * has been moved to allInOne. The latter is hence responsible of
     * deallocating their memory space.
     */
  }
  int value;
  OneType::SP one;
  TwoType::SP two;
  My1 *my1;
  My2 *my2;
  AllInOne allInOne;
  AllInOneWithAggregator allInOneAggregator;
};

BOOST_FIXTURE_TEST_SUITE (space, Fixture)
BOOST_AUTO_TEST_CASE (SymmetricCall)
{
  one->One ();
  BOOST_CHECK_EQUAL (Callback::Instance ().Value (), 1);
  one->Invoke ();
  BOOST_CHECK_EQUAL (Callback::Instance ().Value (), 2);
  one->Peek<ITwo> ()->Two ();
  BOOST_CHECK_EQUAL (Callback::Instance ().Value (), 3);

  two->Two ();
  BOOST_CHECK_EQUAL (Callback::Instance ().Value (), 4);
  two->Invoke ();
  BOOST_CHECK_EQUAL (Callback::Instance ().Value (), 5);
  two->Peek<IOne> ()->One ();
  BOOST_CHECK_EQUAL (Callback::Instance ().Value (), 6);
}

BOOST_AUTO_TEST_CASE (CheckPackSimplePointer)
{
  BOOST_CHECK_EQUAL ((*my1) (), 0);
  BOOST_CHECK_EQUAL ((*my2) (), 0);
  allInOne.Do ();
  BOOST_CHECK_EQUAL ((*my1) (), 1);
  BOOST_CHECK_EQUAL ((*my2) (), 1);
  allInOne.Ref<IOne> ().One ();
  BOOST_CHECK_EQUAL ((*my1) (), 2);
  ConstRefCall (allInOne.Ref<IOne> ());
  RefCall (allInOne.Ref<IOne> ());
}

BOOST_AUTO_TEST_CASE (Aggregator)
{
  BOOST_CHECK_EQUAL (value, 0);
  allInOneAggregator.Do ();
  BOOST_CHECK_EQUAL (value, 2);
  allInOneAggregator.Ref<ITwo> ().Two ();
  BOOST_CHECK_EQUAL (value, 3);
}

BOOST_AUTO_TEST_SUITE_END ()





