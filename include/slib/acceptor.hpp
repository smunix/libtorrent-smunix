/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2011 SMUNIX
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Providence M. Salumu <providence.salumu@smunix.com>
 */


#ifndef ACCEPTOR_HPP__153235
#define ACCEPTOR_HPP__153235

#include <memory>

namespace smunix
{
  template<class T>
  class Acceptor
  {
  public:
    typedef Acceptor ThisType;
    template<class XY>
    struct Rebind { typedef Acceptor<XY> Type; };
  public:
    typedef std::shared_ptr<T> SP;
  public:
    virtual ~Acceptor()
    {
      mPtr.reset ();
    }
    SP const PeekAccepted (void) const
    {
      return mPtr;
    }
    SP PeekAccepted (void)
    {
      return mPtr;
    }
    void PackToBeAccepted (SP a)
    {
      mPtr = a;
    }
  protected:
    Acceptor()
    {}
  private:
    SP mPtr;
  }; // class Acceptor
} // namespace smunix

#define Peek(t) smunix::Acceptor<t>::PeekAccepted ()
#define Pack(t, x) smunix::Acceptor<t>::PackToBeAccepted (x)

#endif /* ACCEPTOR_HPP__153235 */
