/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
* Copyright (c) 2011 SMUNIX
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation;
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Author: Providence M. Salumu <providence.salumu@smunix.com>
*/


#ifndef SINGLETON_HPP__015246
#define SINGLETON_HPP__015246

#include <boost/noncopyable.hpp>
#include <memory>
#include <mutex>
#include <type_traits>

namespace smunix
{

  namespace details
  {
    namespace aux
    {
      template<class R, class S>
      class ConverterTraits
      {
      public:
        typedef ConverterTraits ThisType;
        typedef R ReturnType;
      public:
        static ReturnType Convert (typename std::add_lvalue_reference<S>::type s)
        {
          return *s;
        }
      protected:
        ConverterTraits()
        {}
        virtual ~ConverterTraits()
        {}
      private:
      }; // class ConverterTraits

      template<class S>
      class ConverterTraits<std::shared_ptr<S>, std::shared_ptr<S> >
      {
      public:
        typedef ConverterTraits ThisType;
        typedef std::shared_ptr<S> ReturnType;
      public:
        static ReturnType Convert (typename std::add_lvalue_reference<std::shared_ptr<S> >::type s)
        {
          return s;
        }
      protected:
        ConverterTraits()
        {}
        virtual ~ConverterTraits()
        {}
      private:
      }; // class ConverterTraits

      template<class S>
      class StorageCreatorTraits
      {
      public:
        typedef StorageCreatorTraits ThisType;
        template<class XY>
        struct Rebind { typedef StorageCreatorTraits<XY> Type; };
      public:
        static void Create (typename std::add_lvalue_reference<S>::type s)
        {
          s.reset (new typename S::element_type);
        }
      protected:
        StorageCreatorTraits()
        {}
        virtual ~StorageCreatorTraits()
        {}
      private:
      }; // class StorageCreatorTraits
    } // namespace aux
    template<class Data, class Return, class Storage>
    class Creator
    {
    public:
      typedef Creator ThisType;
      typedef Storage StorageType;
      typedef Data DataType;
      typedef Return ReturnType;
    public:
      static ReturnType Instance (void)
      {
        std::call_once (mCreateOnceFlag, ThisType::Create);
        return aux::ConverterTraits<ReturnType, StorageType>::Convert (mInstance);
      }
    protected:
      Creator()
      {}
      virtual ~Creator()
      {}
    private:
      static void Create (void)
      {
        aux::StorageCreatorTraits<StorageType>::Create(mInstance);
      }
      static std::once_flag mCreateOnceFlag;
      static StorageType mInstance;
    }; // class Creator
    template<class D, class R, class S> typename Creator<D, R, S>::StorageType Creator<D, R, S>::mInstance {nullptr};
    template<class D, class R, class S> std::once_flag Creator<D, R, S>::mCreateOnceFlag;
  } // namespace details


  namespace details
  {
    template<class Ptr>
    class ReturnTraits
    {
    public:
      typedef ReturnTraits ThisType;
      template<class XY>
      struct Rebind { typedef ReturnTraits<XY> Type; };
    public:
      typedef typename Ptr::element_type DataType;
      typedef typename std::add_lvalue_reference<DataType>::type Type;
    protected:

    private:
      ReturnTraits()
      {}
      virtual ~ReturnTraits()
      {}
    }; // class ReturnTraits

    template<class D>
    class ReturnTraits<std::shared_ptr<D> >
    {
    public:
      typedef ReturnTraits ThisType;
      template<class XY>
      struct Rebind { typedef ReturnTraits<XY> Type; };
    public:
      typedef std::shared_ptr<D> Ptr;
      typedef typename Ptr::element_type DataType;
      typedef Ptr Type;
    protected:

    private:
      ReturnTraits()
      {}
      virtual ~ReturnTraits()
      {}
    }; // class ReturnTraits
  } // namespace details

} // namespace smunix


#endif /* SINGLETON_HPP__015246 */
