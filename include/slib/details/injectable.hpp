/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2011 SMUNIX
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Providence M. Salumu <providence.salumu@smunix.com>
 */


#ifndef INJECTABLE_HPP__082012
#define INJECTABLE_HPP__082012

#include <type_traits>

namespace smunix
{
  template<class T>
  struct RemoveInjectable
  {
    typedef T Type;
  };
} // namespace smunix


namespace smunix
{
  template<class A, class IsInjectableOrNot = typename IsInjectable<A>::type>
  struct Creator
  {
    static A * Apply (void)
    {
      return new A;
    }
  };
  template<class A>
  struct Creator<A, std::true_type>
  {
    typedef typename A::SP SP;
    typedef Injector<A> InjectorType;
    static SP Apply (void)
    {
      return A::Create ();
    }
  };
} // namespace smunix

#endif /* INJECTABLE_HPP__082012 */
