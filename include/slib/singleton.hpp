/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
* Copyright (c) 2011 SMUNIX
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation;
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Author: Providence M. Salumu <providence.salumu@smunix.com>
*/

#ifndef SINGLETON_HPP__160500
#define SINGLETON_HPP__160500

#include "details/singleton.hpp"

namespace smunix
{
  template<class D, class R, class S >
  class DefaultStorageCreator : public details::Creator<D, R, S>
  {
  public:
    typedef DefaultStorageCreator ThisType;
  public:

  protected:
    DefaultStorageCreator()
    {}
    virtual ~DefaultStorageCreator()
    {}
  private:

  }; // class DefaultStorageCreator

  template<
    class D,
    class S = std::unique_ptr<D>, 
    class R = typename details::ReturnTraits<S>::Type, 
    template <class, class, class> class StoragePolicy = DefaultStorageCreator >
  class Singleton : public StoragePolicy<D, R, S>, private boost::noncopyable
  {
  protected:
    Singleton()
    {}
    virtual ~Singleton()
    {}
  }; // class Singleton

} // namespace smunix

#endif /* SINGLETON_HPP__160500 */
